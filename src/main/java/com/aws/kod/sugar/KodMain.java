package com.aws.kod.sugar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateName;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;

public class KodMain implements RequestHandler<S3Event, Object> {
	public Collection<String> instanceList;
	public DescribeInstancesResult instanceResult;
	final String zoneString = "Asia/Tokyo";

    public Object handleRequest(S3Event input, Context context) {
        instanceList = new ArrayList<String>();

        @SuppressWarnings("deprecation")
        AmazonEC2Client client = Region.getRegion(Regions.AP_NORTHEAST_1).createClient(AmazonEC2Client.class, null, null);
        instanceResult = client.describeInstances();

		ZonedDateTime nowZonedDt = ZonedDateTime.now(ZoneId.of(zoneString));

		//日本時間で 0-12時→true それ以外→false
        boolean startOrStop = (nowZonedDt.getHour() > 0 && nowZonedDt.getHour() < 12);

        if(startOrStop){
        	runAllInstances(client);
        }else{
        	stopAllInstances(client);
        }
        return null;
    }

    private void stopAllInstances(AmazonEC2Client client){
    	int i = 0;
    	int countStopInstances = 0;
        for (Reservation reservation : instanceResult.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                if (InstanceStateName.Running.toString().equals(instance.getState().getName())) {
                    String instanceId = instance.getInstanceId();
                    List<Tag> hasStopExceptionTag = instance.getTags().stream().filter(tag -> (tag.getKey().equals("stopexception") && tag.getValue().equals("true"))).collect(Collectors.toList());
                    List<Tag> hasDueDateTag = instance.getTags().stream().filter(tag -> (tag.getKey().equals("duedate"))).collect(Collectors.toList());
                    if(isDueDateOver(hasDueDateTag)){
                    	System.out.println(++i + " " +instanceId + "'s duedate is over, so should be Stopped.");
                        countStopInstances++;
                    	instanceList.add(instanceId);
                    }else if(hasStopExceptionTag.isEmpty()){
                        System.out.println(++i + " " +instanceId + " is Running, so should be Stopped.");
                        countStopInstances++;
                    	instanceList.add(instanceId);
                    }else{
                    	System.out.println(++i + " " +instanceId + " is Exception");
                    }
                }
            }
        }
        if(instanceList.size() > 0){
        	StopInstancesRequest stopreq = new StopInstancesRequest().withInstanceIds(instanceList);
        	client.stopInstances(stopreq);
        }
        System.out.println(countStopInstances + " Instances has stopped!!");
    }

    private void runAllInstances(AmazonEC2Client client){
    	int i = 0;
    	int countStartInstances = 0;
        for (Reservation reservation : instanceResult.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                if (InstanceStateName.Stopped.toString().equals(instance.getState().getName())) {
                    String instanceId = instance.getInstanceId();
                    List<Tag> hasStartExceptionTag = instance.getTags().stream().filter(tag -> (tag.getKey().equals("startexception") && tag.getValue().equals("true"))).collect(Collectors.toList());
                    List<Tag> hasDueDateTag = instance.getTags().stream().filter(tag -> (tag.getKey().equals("duedate"))).collect(Collectors.toList());
                    if(isDueDateOver(hasDueDateTag)){
                    	System.out.println(++i + " " +instanceId + "'s duedate is over......");
                    }else if(hasStartExceptionTag.isEmpty()){
                    	System.out.println(++i + " " +instanceId + " is Stopped, so should be activated.");
                    	countStartInstances++;
                    	instanceList.add(instanceId);
                    }else{
                    	System.out.println(++i + " " +instanceId + " is Exception");
                    }
                }
            }
        }
        if(instanceList.size() > 0){
        	StartInstancesRequest startreq = new StartInstancesRequest().withInstanceIds(instanceList);
        	client.startInstances(startreq);
        }
        System.out.println(countStartInstances + " Instances has activated!!");
    }

    private boolean isDueDateOver(List<Tag> hasDueDateTag){
    	if(hasDueDateTag.isEmpty()){
    		return false;
    	}
    	String dueDateTagValue = hasDueDateTag.get(0).getValue();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		Date today  = Calendar.getInstance(TimeZone.getTimeZone(zoneString)).getTime();
		Date dueDate = null;
		try {
			dueDate = sdf.parse(dueDateTagValue + "235959");
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			dueDate = new Date();
			dueDate.setYear(2999);
		}
		if(dueDate.compareTo(today) == -1){
			return true;
		}else{
			return false;
		}
    }
}
